<?php

// Environment settings for local (laptop) virtual machine testing
//

$base_url='http://example.dev';

// These are usually specific per machine (laptop / dev / stage / prod )
$conf['varnish_control_key'] = 'XXXXXXX-xxxx-4e2a-a4cc-442a115d4c3b';

// Only turn on and off modules that are already installed on your system.
// The below settings are typical for my dev machines
$conf = array(
  'drupal_stale_file_threshold' => 172800 ,  
  'GoogleAnalytics' => FALSE,   
  'Qualtrics' => FALSE,
  'Dev_AddThis' => FALSE,
  'modules_on' => array(
    'devel',
    'bulk_export',
    'update',
    'views_ui',
  ),
  'modules_off' => array(
    'shield',
    'ga_remarketing',
    'omniture',
    'varnish',
    'memcache'
  ),
);

// Turn OFF all caching, drupal, memcache, and varnish
// Usually turn off all caching during Front-End Development 

$conf['cache'] = FALSE;
$conf['block_cache'] = FALSE;
$conf['preprocess_css'] = FALSE;
$conf['preprocess_js'] = FALSE;
$conf['page_compression'] = FALSE;
$conf['reverse_proxy'] = FALSE;
$conf['drupal_stale_file_threshold'] = 1;
$conf['omit_vary_cookie'] = FALSE;
$conf['theme_ci_settings']['zen_rebuild_registry'] = TRUE;

  /* Use at your own risk below. It's just further cache disabling
  $vars = array('cache_backends','cache_class_cache_page', 'reverse_proxy_header', 'reverse_proxy_address', 'cache_lifetime', 'cache_default_class', 'cache_class_cache_form');
  foreach ($vars as $var) {
    unset($conf[$var]);
  }
   */

