<?php
/**
 *  Original Author: Irakli Nadareishvili https://drupal.org/user/96826
 *  Modified : Dan Shumaker https://drupal.org/user/86074
 *
 * Environment-Specific configuration manager
 */
class DevEnv {

  private static $currentEnvironment = null;

  /**
   * @static
   *
   * Static initializer.
   */
  public static function init() {

    $envname_server = !empty($_SERVER['DRUPAL_ENV_NAME']) ? $_SERVER['DRUPAL_ENV_NAME'] : "";
    $envname_shell = !empty($_ENV['DRUPAL_ENV_NAME']) ? $_ENV['DRUPAL_ENV_NAME'] : "";
    $envname = !empty($envname_server) ? $envname_server : $envname_shell;

    if(empty($envname)) {
      $envname = 'nothing';
    }

    $configFilePath = __DIR__ . DIRECTORY_SEPARATOR . $envname . ".inc";
    self::$currentEnvironment = $envname;

    return $configFilePath;
  }

  public static function getCurrent() {
    return self::$currentEnvironment;
  }

}

$drupalEnvConfigFilePath = DevEnv::init();

// Not checking file_exists here because we need every bit of performance and file_exists() is not that fast.
if (DevEnv::getCurrent() != 'nothing') {
  require($drupalEnvConfigFilePath);
}
