<?php

// Environment settings for local (laptop) virtual machine testing
//

$base_url='http://example.prod';

// These are usually specific per machine (laptop / dev / stage / prod )
$conf['varnish_control_key'] = 'XXXXXXX-xxxx-4e2a-a4cc-442a115d4c3b';

// Only turn on and off modules that are already installed on your system.
// The below settings are typical for my dev machines
$conf = array(
  'drupal_stale_file_threshold' => 172800 ,  
  'GoogleAnalytics' => TRUE,   
  'Qualtrics' => TRUE,
  'Dev_AddThis' => TRUE,
  'modules_on' => array(
    'ga_remarketing',
    'omniture',
    'varnish',
    'memcache'
  ),
  'modules_off' => array(
    'bulk_export',
    'update',
    'views_ui',
    'devel',
    'shield',
  ),
);

// Memcache setup
$conf['cache_backends'][] = '/sites/all/modules/contrib/memcache/memcache.inc';
$conf['cache_default_class'] = 'MemCacheDrupal';
$conf['cache_class_cache_form'] = 'DrupalDatabaseCache';
$conf['memcache_key_prefix'] = 'dan_debian_laptop_ci';
//$conf['memcache_options'] = array( Memcached::OPT_BINARY_PROTOCOL => TRUE);

// Turn on page caching so varnish works
$conf['cache'] = TRUE;
$conf['block_cache'] = TRUE;
$conf['preprocess_css'] = TRUE;
$conf['preprocess_js'] = TRUE;
$conf['page_compression'] = TRUE;
$conf['drupal_stale_file_threshold'] = 17280;
$conf['theme_ci_settings']['zen_rebuild_registry'] = FALSE;

// Varnish Setup
$conf['cache_backends'][] = '/sites/all/modules/contrib/varnish/varnish.cache.inc';
$conf['cache_class_cache_page'] = 'VarnishCache';
$conf['page_cache_invoke_hooks'] = FALSE;
$conf['reverse_proxy'] = TRUE;
$conf['cache_lifetime'] = 0;
$conf['page_cache_maximum_age'] = 21600;
$conf['reverse_proxy_header'] = 'HTTP_X_FORWARDED_FOR';
$conf['reverse_proxy_addresses'] = array('127.0.0.1');
$conf['omit_vary_cookie'] = TRUE;

