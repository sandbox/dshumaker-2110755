<?php
/**
 * 
 */
function dev_env_admin_settings() {

  global $conf;

  $form = array();
  require_once 'environments.inc';
  $cust_env= DevEnv::getCurrent();
  $form['current_env'] = array(
    '#markup' => '<p>The current custom environment loaded is <h1>' . $cust_env . '</h1></p><br>',
  );
  $form['config'] = array(
    '#type' => 'fieldset',
    '#title' => 'Current Configuration Settings',
    '#description' => 'For debugging purposes',
    '#collapible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['config']['krumo_config'] = array(
    '#markup' => kprint_r($conf, true),
  );
  return $form;
}

